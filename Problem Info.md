# Problem Discription

Observability data can be any of the following:
I). Telco-Cloud Infrastructure Metrics (servers)
- H/W Level
- OS Level
- Virtualization Level
II). Metrics/Statistics from Physical Network Elements.
III). Application (virtualized Network functions) Metrics
IV). System logs (Servers, Applications, etc.)
V). Metrics/Statistics from centralized orchestration systems (VIM, SDN-Controller, VNFM, NFVO, etc.)
VI). Metrics/Statistics from other control-plane services

Availability of these data for AI/ML researchers, who are not part of the Telco, is very difficult. To solve this availability issue, one approach is to generate synthetic observability data. In this project, we propose to generate this synthetic observability data using GANs. For this first round only Telco-Cloud Infrastructure Metrics will be considered.

## Evaluation Criteria

1. Submission of the GANs implementation as Python (or any other language) program.
2. Least error from the discriminator.

## Contact

Lei Huang: huangleiyjy@chinamobile.com
Sridhar K. N. Rao: sridharkn@u.nus.edu

